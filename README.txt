INTRODUCTION
------------

This Drupal module performs OWASP-compliant login velocity check by comparing
geolocation of user's current IP address against geolocation of same
user's IP address during previous login session within time period specified in 
module's settings.

REQUIREMENTS
------------

None.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration > People > Permissions:

   - Use the administration pages and help (System module)

     The top-level administration categories require this permission to be
     accessible. The administration menu will be empty unless this permission
     is granted.

   - Access administration menu

     Users in roles with the "Access administration menu" permission will see
     the administration menu at the top of each page.

   - Display Drupal links

     Users in roles with the "Display drupal links" permission will receive
     links to drupal.org issue queues for all enabled contributed modules. The
     issue queue links appear under the administration menu icon.

 * Customize the menu settings in Administration > Configuration and modules >
   Administration > Administration menu.

 * To prevent administrative menu items from appearing twice, you may hide the
   "Management" menu block.

CUSTOMIZATION
-------------

* To override the default "OWASP login velocity check time interval (minutes)"
or "OWASP login velocity check error message":

you can do by using this link:
/admin/config/system/owasp-check
to settings form.
